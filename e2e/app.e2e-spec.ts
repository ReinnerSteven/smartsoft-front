import { SmartSoftPage } from './app.po';

describe('smart-soft App', () => {
  let page: SmartSoftPage;

  beforeEach(() => {
    page = new SmartSoftPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});

import { Injectable } from '@angular/core';
import { Http, Request , Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';

@Injectable()


export class AppServices {


    public urlApp = "http://localhost:3100/api";

	

	constructor(private _http:Http){ }

	/////////////////////////////////  Servicio de Country  /////////////////////////////////

	getAllItems(){
		let headers = new Headers({ "Content-Type": "application/json"});
		let options = new RequestOptions({ headers:headers});
		return this._http.get(this.urlApp+"/items",options)
		.map(res => res.json());
	}

	createItem(data){
		let headers = new Headers({ "Content-Type": "application/json"});
		let options = new RequestOptions({ headers:headers});
		let body = JSON.stringify(data);
		return this._http.post(this.urlApp+"/items",body,options).map(res => res.json());
	}

	deleteItem(id){

		let headers = new Headers({ "Content-Type": "application/json"});
		let options = new RequestOptions({ headers:headers});
		return this._http.delete(this.urlApp+"/items/"+id,options).map(res => res.json());
	}
    
    getItemById(id){

		let headers = new Headers({ "Content-Type": "application/json"});
		let options = new RequestOptions({ headers:headers});
		return this._http.get(this.urlApp+"/items/"+id,options).map(res => res.json());
	}

	updateItem(data,id){

		let headers = new Headers({ "Content-Type": "application/json"});
		let options = new RequestOptions({ headers:headers});
		let body = JSON.stringify(data);
		return this._http.put(this.urlApp+"/items/"+id,body,options).map(res => res.json());
	}
}
import { Component,OnInit,OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppServices } from '../../../Service/appServices';
import { JsonApi } from '../../../Model/model';
import {Location} from '@angular/common';
//json-edit    
declare var JSONEditor: any;
declare var $: any;

@Component({
    selector: 'inferenciador-app',
    templateUrl: './inferenciador.html',
    providers: [AppServices,JsonApi]
})

export class InferenciadorComponent implements OnInit {

    private listData = [];
    private editor: any;
    private messageService = ""

    constructor(private _route: ActivatedRoute,private _services: AppServices, private _json:JsonApi,private _location: Location){

    }
    
    getItems(){
        this._services.getAllItems().subscribe(data => {            
           this.listData = data.data;        
        })
    }
    
    ngOnInit() {
        

        this.messageService = "";
        var container = document.getElementById("jsoneditor");
        var options = {
            mode: 'tree'
        };
        this.editor = new JSONEditor(container, options);
        
         this.getItems();
        
    }
    
    validate(){

    }

}





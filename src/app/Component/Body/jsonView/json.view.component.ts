import { Component,OnInit,OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppServices } from '../../../Service/appServices';
import { JsonApi } from '../../../Model/model';
import {Location} from '@angular/common';
//json-edit    
declare var JSONEditor: any;
declare var $: any;

@Component({
    selector: 'json-app',
    templateUrl: './json.view.component.html',
    providers: [AppServices,JsonApi]
})

export class JSonComponent implements OnInit {

    private editor: any;
    private sub: any;
    private create = false;
    private titleButton = "Crear reigla";
    private idItem = "";
    private messageModal = ""
    private messageService = ""

constructor(private _route: ActivatedRoute,private _services: AppServices, private _json:JsonApi,private _location: Location){

    }
    

    ngOnInit() {
        
        this.loadJsonEdit() 
     
        
        
        this.sub = this._route.params.subscribe(params => {
            this.idItem  = params['id'];
            if(this.idItem  != undefined){
                this.getItemById(this.idItem);
                this.create = false;
                this.titleButton = "Modificar reigla"
            }else{
                this.create = true;                
                this.titleButton = "Crear reigla"
            }
        }); 
    }
    
    //Cargar el json-edit
    loadJsonEdit(){
        this.messageService = "";
        var container = document.getElementById("jsoneditor");
        var options = {
            mode: 'tree'
        };
        this.editor = new JSONEditor(container, options);
    }

    //Cargar primer ejemplo
    loadExample(){    
        $('#modalMessage').modal('hide');
        this.editor.set(this._json.jsonExample);
    }
    
    //Cargar segundo ejemplo
    loadExample2(){    
        $('#modalMessage').modal('hide');
        this.editor.set(this._json.jsonExample2);
    }
    
    //Get reigla por id
    getItemById(id){
        this._services.getItemById(id).subscribe(data => {
            this.editor.set(data.data.jsonObj);
        })          
    }
    
    //Crear Reigla
    ruleElementA(item,nameRule){
    
 
        if(item == undefined ){
            this.messageModal = 'Error en el formato JSON, por favor verifique la estructura del archivo, puede verificar la estrucutra viendo el siguiente ejemplo';
                $('#modalMessage').modal('show');
            return;
        }

        if(item.type == "fact")
            nameRule += " " + item.field;

        if(item.type == "constant")        
            nameRule += " " + item.value;
        
        if(item.type == "add"){            
            var count = 1;
            item.inputs[0].forEach(function(valueItem){                
                nameRule += " " + valueItem.field;
                if(count != item.inputs[0].length){
                    count ++;
                    nameRule += " +"
                }
            })
        }
        
         if(item.type == "mul"){            
            var count = 1;
            item.inputs[0].forEach(function(valueItem){
                
                nameRule += " " + valueItem.field;
                if(count != item.inputs[0].length){
                    count ++;
                    nameRule += " *"
                }
            })
        }
        
         if(item.type == "div"){            
            var dataItemA = item.a[0];
            var dataItemB = item.b[0];
            nameRule += " " + dataItemA[0].field;
            nameRule += " /"
            nameRule += " " + dataItemB[0].field;
          
        }
        
        return nameRule;
    }
    
    //Crear Reigla
    createConditionCompare(element){
 
        var nameRule = "";
        
        nameRule = this.ruleElementA(element.a[0][0],nameRule);
        nameRule += " " + element.condition;
        nameRule = this.ruleElementA(element.b[0][0],nameRule);
        return nameRule;

    }
    
    
    typeConditionElement(condition){
        switch(condition){
                case "and":
                    return "&&"
                case "or":
                    return "||"
        }
    }    
    
    //Guardar la informacion
    saveInfo(){

        // get json
        var json = this.editor.get();
          
        if(json.condition == undefined){
            this.messageModal = 'Error en el formato JSON, por favor verifique la estructura del archivo, puede verificar la estrucutra viendo el siguiente ejemplo';
                $('#modalMessage').modal('show');
            return;
        }
        
        if(json.condition.type == undefined || json.condition.inputs == undefined){
            this.messageModal = 'Error en el formato JSON, por favor verifique la estructura del archivo, puede verificar la estrucutra viendo el siguiente ejemplo';
                $('#modalMessage').modal('show');
            return;
        }
        
        var condition = json.condition;
        var typeCondition = condition.type;
        var inputsCondition = condition.inputs;
        const self = this;
        
        var nameRule = "";
        
        if(inputsCondition.length > 0){

            inputsCondition.forEach(function(itemsInput) {
                

                var count = 1;
         
                if(itemsInput == undefined ){
                    this.messageModal = 'Error en el formato JSON, por favor verifique la estructura del archivo, puede verificar la estrucutra viendo el siguiente ejemplo';
                        $('#modalMessage').modal('show');
                    return;
                }
                
                if(itemsInput.length>0){
                     itemsInput.forEach(function(element) {
                        if(element.type == "compare") 
                            nameRule += self.createConditionCompare(element)

                        if(count != itemsInput.length){
                            count++;
                            nameRule += " " + self.typeConditionElement(typeCondition);
                        }
                    })

                }else{
 
                    if(itemsInput.type == undefined){
                        this.messageModal = 'Error en el formato JSON, por favor verifique la estructura del archivo, puede verificar la estrucutra viendo el siguiente ejemplo';
                        $('#modalMessage').modal('show');
                        return;
                    }else{
                        nameRule += self.createConditionCompare(itemsInput)
                    }
                }
            });
            
            var obj = {
                name:json.name,
                condition: nameRule,
                jsonObj:json
            }


            if(obj.name == "" || obj.name == undefined){
                this.messageModal = 'No existe el nombre para esta reigla, por favor verifique la estructura del archivo, puede verificar la estrucutra viendo el siguiente ejemplo';
                $('#modalMessage').modal('show');
                return;
            }


            if(obj.condition == "" || obj.condition == undefined ){
                this.messageModal = 'No se pudo verificar la condicion, por favor verifique la estructura del archivo, puede verificar la estrucutra viendo el siguiente ejemplo';
                    $('#modalMessage').modal('show');
                return;
            }

            //Realizar el servicio de CREACION O EDICION
            if(this.create){
                this._services.createItem(obj).subscribe(data => {
                    this.messageService = "Reigla creada correctamente"
                })          

            }else{

                this._services.updateItem(obj,this.idItem).subscribe(data => {
                    this.messageService = "Se actualizo la reigla correctamente"
                })
            }

        }else{
            
            this.messageModal = 'No hay elementos de entrada, por favor verifique la estructura del archivo, puede verificar la estrucutra viendo el siguiente ejemplo';
            $('#modalMessage').modal('show');
            return;
        }  
    }
    
    OnDestroy(){
        this.sub.unsubscribe();
    }
}





import { Component, OnInit } from '@angular/core';
import { Routes, Router,  RouterModule } from '@angular/router'
import { AppServices } from '../../../Service/appServices';


declare var io: any;



@Component({
    selector: 'home-app',
    templateUrl: './home.component.html',
    providers: [AppServices]
})

export class HomeComponent implements OnInit {    
    private listData = [];
    
    
    constructor(private _router :Router, private _services: AppServices){

    }   
    
    //Cambiar a vista de edicion
    editRule(idItem){
        let url = "/json-view/" + idItem;	
        this._router.navigate([url]);
    }
    
    //Crear una reigla
    createRule(){
            let url = "/json-view";	
            this._router.navigate([url]);
    }
    
    //Eliminar una reigla
    deleteItem(idItem){
        
        this._services.deleteItem(idItem).subscribe(data => {            
            this.getItems();
        })
    }
    
    //Todos las reiglas creadas
    getItems(){
        this._services.getAllItems().subscribe(data => {            
           this.listData = data.data;        
        })
    }
    
    ngOnInit() {
        this.getItems();
    }

}

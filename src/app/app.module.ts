import { NgModule }      from '@angular/core';
import { FormsModule, ReactiveFormsModule}   from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule  }     from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent} from './Component/Header/header.component'
import { FooterComponent} from './Component/Footer/footer.component'
import { BodyComponent} from './Component/Body/body.component'
import { JSonComponent} from './Component/Body/jsonView/json.view.component'
import { HomeComponent} from './Component/Body/home/home.component'
import { InferenciadorComponent} from './Component/Body/inferenciador/inferenciador'

/*NotFound*/

import { NotFoundComponent} from './Component/Body/NotFound/not.found.component'

/*Router*/
import { RouterModule } from '@angular/router';
import { routing, appRoutingProviders } from './Router/routerComponent';

@NgModule({
    declarations: [
        AppComponent,HeaderComponent,BodyComponent,JSonComponent,HomeComponent,NotFoundComponent,InferenciadorComponent,FooterComponent
    ],
    imports: [
        BrowserModule, 
        FormsModule, 
        HttpModule,
        JsonpModule,
        ReactiveFormsModule,
        routing
    ],
    exports: [
        RouterModule
    ],
    providers: [ 
        appRoutingProviders
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }


import { ModuleWithProviders} from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { HomeComponent} from '../Component/Body/home/home.component'
import { InferenciadorComponent} from '../Component/Body/inferenciador/inferenciador'
import { JSonComponent} from '../Component/Body/jsonView/json.view.component'
import { NotFoundComponent} from '../Component/Body/NotFound/not.found.component'


const appRoutes: Routes = [


	{ path: '', redirectTo: '/home', pathMatch: 'full' },
	{ path: 'home', 		component: HomeComponent },
    { path: 'inferenciador', 		component: InferenciadorComponent },
    { path: 'json-view/:id', 		component: JSonComponent },
    { path: 'json-view', 		component: JSonComponent },
    { path: '**', component: NotFoundComponent }
	

];

export const appRoutingProviders: any[] = [];
export const routing:ModuleWithProviders = RouterModule.forRoot(appRoutes);

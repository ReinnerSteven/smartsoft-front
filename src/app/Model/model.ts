
import { Injectable } from '@angular/core';

@Injectable()


export class JsonApi {

	constructor(){ }
    
    public jsonExample = {
          "idRule": null,
          "name": "Regla de prueba",
          "condition": {
            "type": "and",
            "inputs": [
              [
                {
                  "type": "compare",
                  "a": [
                    [
                      {
                        "type": "fact",
                        "field": "Monto"
                      }
                    ]
                  ],
                  "condition": ">",
                  "b": [
                    [
                      {
                        "type": "constant",
                        "type_value": "int",
                        "value": "1400"
                      }
                    ]
                  ]
                },
                {
                  "type": "compare",
                  "a": [
                    [
                      {
                        "type": "fact",
                        "field": "Comercio"
                      }
                    ]
                  ],
                  "condition": "==",
                  "b": [
                    [
                      {
                        "type": "fact",
                        "field": "Mac"
                      }
                    ]
                  ]
                }
              ]
            ]
          }
        };
    
    
    public jsonExample2 = {
  "idRule": null,
  "name": "Regla de prueba",
  "condition": {
    "type": "and",
    "inputs": [
      {
        "type": "compare",
        "a": [
          [
            {
              "type": "add",
              "inputs": [
                [
                  {
                    "type": "fact",
                    "field": "a"
                  },
                  {
                    "type": "fact",
                    "field": "b"
                  }
                ]
              ]
            }
          ]
        ],
        "condition": "<",
        "b": [
          [
            {
              "type": "div",
              "a": [
                [
                  {
                    "type": "fact",
                    "field": "c"
                  }
                ]
              ],
              "b": [
                [
                  {
                    "type": "fact",
                    "field": "d"
                  }
                ]
              ]
            }
          ]
        ]
      }
    ]
  }
}

}